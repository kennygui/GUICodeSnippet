//
//  NSObject+GQ.m
//  RemoteCall
//
//  Created by Katsura on 3/3/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//

#import "NSObject+GUI.h"
#import <objc/runtime.h>

@implementation NSObject (GUI)

-(void)setMyInfo:(NSDictionary *)newUserInfo
{
    
    objc_setAssociatedObject(self, @"myInfo", newUserInfo,OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(id)myInfo
{
    
    return objc_getAssociatedObject(self, @"myInfo");
    
}
@end
