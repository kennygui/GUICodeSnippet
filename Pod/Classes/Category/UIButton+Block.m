//
//  UIButton+Block.m
//  CloudNurse
//
//  Created by Katsura on 7/15/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//

#import "UIButton+Block.h"

@implementation UIButton(Block)
//@dynamic event;
static char identifier = 'g';

- (void)handleControlEvent:(UIControlEvents)event withBlock:(ActionCodeBlock)codeBlock {
    
    objc_setAssociatedObject(self, &identifier, codeBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self addTarget:self action:@selector(callActionCodeBlock:) forControlEvents:event];
    
}


- (void)callActionCodeBlock:(id)sender {
    ActionCodeBlock codeBlock = (ActionCodeBlock)objc_getAssociatedObject(self, &identifier);
    if (codeBlock) {
        codeBlock();
    }
}


@end
