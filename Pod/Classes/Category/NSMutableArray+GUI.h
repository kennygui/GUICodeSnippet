//
//  NSMutableArray+GQ.h
//  netBarRemoteCall
//
//  Created by Katsura on 3/24/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (GUI)
/**
 *  插入一个数组
 *
 */
- (void)insertArray:(NSArray *)newAdditions atIndex:(NSUInteger)index;

@end
