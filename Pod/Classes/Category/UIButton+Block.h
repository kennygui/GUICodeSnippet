//
//  UIButton+Block.h
//  CloudNurse
//
//  Created by Katsura on 7/15/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>


typedef void (^ActionCodeBlock)();

@interface UIButton(Block)

- (void) handleControlEvent:(UIControlEvents)controlEvent withBlock:(ActionCodeBlock)action;

//@property (readonly) NSMutableDictionary *event;

@end


