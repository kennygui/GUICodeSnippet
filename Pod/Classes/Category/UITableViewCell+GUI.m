//
//  UITableViewCell+GUI.m
//  Pods
//
//  Created by kenny on 3/29/16.
//
//

#import "UITableViewCell+GUI.h"

@implementation UITableViewCell (GUI)

+ (void) registerTableView:(UITableView*)tableview nibIdentifier:(NSString*)identifier{
    
    [tableview registerNib:[self nibWithIdentifier:identifier] forCellReuseIdentifier:identifier];
    
}

+ (CGFloat) cellHeightFromModel:(id)model atIndexPath:(NSIndexPath*)path{
    if (!model) {
        return 0.0f;
    }
    return 44.0f;
    
}

-(void)configCell:(UITableViewCell*)cell withModel:(id)model atIndexPath:(NSIndexPath*)indexPath{
    // override this
    
}


#pragma mark - private
+ (UINib *)nibWithIdentifier:(NSString *)identifier
{
    return [UINib nibWithNibName:identifier bundle:[NSBundle mainBundle]];
}

@end
