//
//  UITextField+GUI.m
//  HuHuYunYu
//
//  Created by Katsura on 8/6/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//

#import "UITextField+GUI.h"

@implementation UITextField (GUI)



+(UIView*)textFieldsLeftViewWthImageName:(NSString*)imageName bounds:(CGRect)bounds iconFrame:(CGRect)frame{
    UIView * usernameLeftView = [[UIImageView alloc]init];
    usernameLeftView.bounds = CGRectMake(0, 0, 20, bounds.size.height);
    
    UIImageView * userNameIconView = [[UIImageView alloc]init];
    userNameIconView.image = [UIImage imageNamed:imageName];
    //bounds only ,frame is not available
    userNameIconView.frame = frame;
    
    UIView * seperatorView = [[UIView alloc]init];
    seperatorView.backgroundColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:0.5];
    CGFloat seperatorX = bounds.size.width -1;
    CGFloat seperatorW = 1;
    CGFloat seperatorH = bounds.size.height/2;
    CGFloat seperatorY = (bounds.size.height - seperatorH )/2;
    seperatorView.frame = CGRectMake(seperatorX,seperatorY,seperatorW,seperatorH);
    
    
    [usernameLeftView addSubview:userNameIconView];
    [usernameLeftView addSubview:seperatorView];
    return  usernameLeftView;


}

@end
