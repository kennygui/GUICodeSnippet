//
//  UIBarButtonItem+GQ.h
//  DoubleLineTracking
//
//  Created by guiqing on 12/6/14.
//  Copyright (c) 2014 guiqing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (GUI)
/**
 *  快速创建一个显示图片,高亮图片的item
 *
 *  @param action   监听方法
 */
+ (UIBarButtonItem *)itemWithIcon:(NSString *)icon highIcon:(NSString *)highIcon target:(id)target action:(SEL)action;
/**
 *  显示图片的 item
 *
 */
+ (UIBarButtonItem *)itemWithImageName:(NSString *)imageName target:(id)target action:(SEL)action;
@end
