//
//  UIView+GQ.h
//  combine_api
//
//  Created by katsura on 12/30/14.
//  Copyright (c) 2014 guiqing. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIView (GUI)
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGSize size;


/**
 *  打印当前 View 的 frame
 */

-(void)printFrame;
@end
