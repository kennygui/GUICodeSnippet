//
//  NSString+GQ.h
//  combine_api
//
//  Created by ketsurake on 15-1-5.
//  Copyright (c) 2015年 guiqing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//获取文件MD5必须定义
#define FileHashDefaultChunkSizeForReadingData 1024*8


@interface NSString (GUI)

-(CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize;
/**
 *  返回A字符串中特定字符之"前"的字符串
 *
 */
+(NSString*)substring:(NSString*)originString ToCharacter:(NSString*)character;
/**
 *  32bit MD5 encryption with salt
 *
 */
- (NSString *)myMD5;

/**
 *  32bit MD5 encryption
 *
 *  @return 32bit MD5 encryption
 */
- (NSString *)MD5;

/**
 *  SHA1 encryption
 *
 *  @return encryption
 */
- (NSString *)SHA1;

/**
 *  使用正则表达式查找结果
 *
 *  @param pattern 正则表达式
 *
 *  @return 结果数组
 */
-(NSArray*)regularExpressionWithPatten:(NSString*)pattern;

/**
 *  获取文件 MD5 值
 *
 *  @param path 文件路径
 *
 *  @return MD5
 */
+(NSString*)getFileMD5WithPath:(NSString*)path;

@end
