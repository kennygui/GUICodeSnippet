//
//  NSObject+GQ.h
//  RemoteCall
//
//  Created by Katsura on 3/3/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (GUI)

@property (nonatomic,retain)NSDictionary * myInfo;

@end
