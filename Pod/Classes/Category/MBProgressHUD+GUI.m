

#import "MBProgressHUD+GUI.h"

@implementation MBProgressHUD (GUI)
#pragma mark 显示信息
/**
 *  显示一个提示框
 *
 *  @param text 文字内容
 *  @param icon 图标
 *  @param view 目标视图
 */
+ (void)show:(NSString *)text icon:(NSString *)icon view:(UIView *)view
{
    //如果没有传递目标视图,那么取得窗口的最上面一层的控制器
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = text;
    // 设置图片
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MBProgressHUD.bundle/%@", icon]]];
    // 再设置模式
    hud.mode = MBProgressHUDModeCustomView;
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    // 1秒之后再消失
    [hud hide:YES afterDelay:0.7];
}

#pragma mark 显示错误信息
/**
 *  显示错误信息
 *
 *  @param error 错误描述
 *  @param view  目标视图
 */
+ (void)showError:(NSString *)error toView:(UIView *)view{
    [self show:error icon:@"error.png" view:view];
}

+ (void)showSuccess:(NSString *)success toView:(UIView *)view
{
    [self show:success icon:@"success.png" view:view];
}

#pragma mark 显示一些信息
/**
 *  显示信息
 *
 *  @param message 信息描述
 *  @param view    目标视图
 *
 *  @return 带蒙板的HUD
 */
+ (MBProgressHUD *)showMessage:(NSString *)message toView:(UIView *)view {
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = message;
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    // YES代表需要蒙版效果
    hud.dimBackground = YES;
    return hud;
}

/**
 *  显示成功信息
 *
 *  @param success 信息
 */
+ (void)showSuccess:(NSString *)success
{
    [self showSuccess:success toView:nil];
}
/**
 *  显示失败信息
 *
 *  @param error 信息
 */
+ (void)showError:(NSString *)error
{
    [self showError:error toView:nil];
}
/**
 *  显示提示信息
 *
 *  @param message 信息提示
 *
 *  @return 提示信息
 */
+ (MBProgressHUD *)showMessage:(NSString *)message
{
    return [self showMessage:message toView:nil];
}

/**
 *  隐藏 HUD
 *
 *  @param view 目标视图
 */
+ (void)hideHUDForView:(UIView *)view
{
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    [self hideHUDForView:view animated:YES];
}

/**
 *  隐藏HUD
 */
+ (void)hideHUD
{
    [self hideHUDForView:nil];
}

+ (void)showToastTip:(NSString *)message onView:(UIView *)view yOffet:(CGFloat)offset autoHide:(BOOL)hideFlag
{
    MBProgressHUD *toastTipView = [MBProgressHUD showHUDAddedTo:view animated:YES];
    toastTipView.animationType = MBProgressHUDAnimationFade;
    toastTipView.mode = MBProgressHUDModeText;
    toastTipView.labelText = message;
    toastTipView.margin = 10.0f;
    toastTipView.yOffset = offset;
    toastTipView.cornerRadius = 3.0f;
    toastTipView.removeFromSuperViewOnHide = YES;
    
    if (hideFlag) {
        [toastTipView hide:YES afterDelay:1.5f];
    }
}

@end
