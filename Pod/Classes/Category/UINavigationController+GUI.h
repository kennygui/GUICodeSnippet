//
//  UINavigationController+GUI.h
//  CloudNurse
//
//  Created by Katsura on 5/27/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (GUI)

- (void)pushViewController: (UIViewController*)controller animatedWithTransition: (UIViewAnimationTransition)transition;

- (UIViewController*)popViewControllerAnimatedWithTransition:(UIViewAnimationTransition)transition;

@end
