//
//  UILabel+GQ.m
//  CloudNurse
//
//  Created by katsura on 1/22/15.
//  Copyright (c) 2015 newone. All rights reserved.
//

#import "UILabel+GUI.h"

@implementation UILabel (GUI)

+(UILabel*)labelWithString:(NSString*)str FontSize:(CGFloat)fontSize{

    UILabel *label = [[UILabel alloc]init];
    //set infinite line numbers
    label.numberOfLines = 0;
    //set text
    label.text = str;
    //set font
    label.font = [UIFont systemFontOfSize:fontSize];
    

    return label;
}


- (CGSize)boundingRectWithSize:(CGSize)size
{
    NSDictionary *attribute = @{NSFontAttributeName: self.font};
    
    CGSize retSize = [self.text boundingRectWithSize:size
                                             options:\
                      NSStringDrawingTruncatesLastVisibleLine |//绘制到最后一个可见行
                      NSStringDrawingUsesLineFragmentOrigin |
                      NSStringDrawingUsesFontLeading
                                          attributes:attribute
                                             context:nil].size;
    
    return retSize;
}

-(CGSize)boundingRectWithSize:(CGSize)size font:(UIFont*) font
{
    NSDictionary *attribute = @{NSFontAttributeName: font};
    
    CGSize retSize = [self.text boundingRectWithSize:size
                                             options:\
                      NSStringDrawingTruncatesLastVisibleLine |//绘制到最后一个可见行
                      NSStringDrawingUsesLineFragmentOrigin |
                      NSStringDrawingUsesFontLeading
                                          attributes:attribute
                                             context:nil].size;
    
    return retSize;

}
@end
