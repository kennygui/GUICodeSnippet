//
//  UITextField+GUI.h
//  HuHuYunYu
//
//  Created by Katsura on 8/6/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (GUI)


/**
 *  创建一个textField的leftView
 *
 *
 *  @return leftView
 */
+(UIView*)textFieldsLeftViewWthImageName:(NSString*)imageName bounds:(CGRect)bounds iconFrame:(CGRect)frame;

@end
