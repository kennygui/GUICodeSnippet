

#import "MBProgressHUD.h"

@interface MBProgressHUD (GUI)

/**
 *  显示一个自定义提示框
 *
 *  @param text 文字内容
 *  @param icon 图标
 *  @param view 目标视图
 */
+ (void)show:(NSString *)text icon:(NSString *)icon view:(UIView *)view;
/**
 *  打印成功提示
 *
 *  @param success 成功描述
 *  @param view    目标视图
 */
+ (void)showSuccess:(NSString *)success toView:(UIView *)view;
/**
 *  打印错误信息
 *
 *  @param error 错误描述
 *  @param view  目标视图
 */
+ (void)showError:(NSString *)error toView:(UIView *)view;


/**
 *  显示消息
 *
 *  @param message 消息描述
 *  @param view    目标视图
 *
 *  @return HUD对象
 */
+ (MBProgressHUD *)showMessage:(NSString *)message toView:(UIView *)view;

/**
 *  打印成功信息
 *
 *  @param success 成功描述
 */
+ (void)showSuccess:(NSString *)success;
/**
 *  打印错误信息
 *
 *  @param error 错误描述
 */
+ (void)showError:(NSString *)error;

/**
 *  显示消息
 *
 *  @param message 消息描述
 *
 *  @return HUD对象
 */
+ (MBProgressHUD *)showMessage:(NSString *)message;
/**
 *  隐藏HUD
 *
 *  @param view 目标视图
 */
+ (void)hideHUDForView:(UIView *)view;
/**
 *  隐藏HUD
 */
+ (void)hideHUD;

+ (void)showToastTip:(NSString *)message onView:(UIView *)view yOffet:(CGFloat)offset autoHide:(BOOL)hideFlag;

@end
