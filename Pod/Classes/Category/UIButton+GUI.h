//
//  UIButton+GQ.h
//  CloudNurse
//
//  Created by katsura on 1/23/15.
//  Copyright (c) 2015 newone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (GUI)

+(UIButton*)buttonWithImage:(NSString*)normalImage highlightedImage:(NSString*)highlightedImage target:(id)target action:(SEL)action;


/**
 *  添加按钮
 *
 *  @param icon  图标
 *  @param title 标题
 */
+(UIButton *)setupBtnWithIcon:(NSString *)icon title:(NSString *)title bgColor:(UIColor *)bgColor;


/**
 *  创建纯文字按钮
 *
 *  @param titleString       标题
 *  @param highlightedString 高亮标题
 *  @param target            target
 *  @param action            action
 *
 *  @return button
 */
+(UIButton*)buttonWithTitle:(NSString*)titleString highlightTitle:(NSString*)highlightedString target:(id)target action:(SEL)action;
@end
