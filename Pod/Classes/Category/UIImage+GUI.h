//
//  UIImage+GQ.h
//  DoubleLineTracking
//
//  Created by guiqing on 12/6/14.
//  Copyright (c) 2014 guiqing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (GUI)

/**
 *  适配 iOS6 iOS7的图片方法
 *
 *
 */
+ (UIImage*)imageAdjustedWithName:(NSString*)name;
/**
 *  拉伸图片显示的方法
 *
 *  @param name 图片名称
 *
 *  @return 拉伸好的图片
 */
+ (UIImage *)imageResizeWithName:(NSString *)name;
/**
 *  拉伸图片的方法
 *
 *  @param name 图片名称
 *  @param left 左侧开始
 *  @param top  上侧开始
 *
 */
+ (UIImage *)imageResizedImageWithName:(NSString *)name left:(CGFloat)left top:(CGFloat)top;
/**
 *  利用颜色生成一张图片
 *
 *  @param color 颜色
 *
 *  @return 图片
 */
+ (UIImage *)imageWithColor:(UIColor *)color;
/**
 *  生成头像(使用UIImage)
 *
 *  @param icon   头像图片名称
 *  @param border 头像边框大小
 *  @param color  头像边框的颜色
 *
 *  @return 生成好的头像
 */
+ (instancetype)roundCornerImageWithImage:(UIImage *)icon border:(NSInteger)border color:(UIColor *)color;

/**
 *  生成头像(使用图像文件名)
 *
 *  @param icon   头像图片名称
 *  @param border 头像边框大小
 *  @param color  头像边框的颜色
 *
 *  @return 生成好的头像
 */
+ (instancetype)roundCornerImageWithName:(NSString *)iconString border:(NSInteger)border color:(UIColor *)color;
/**
 *  根据矩形创建一个切割图片
 */
+(instancetype)imageWithRect:(CGRect)rect imageName:(NSString*)imgStr;
/**
 *  毛玻璃效果图片
 */
- (UIImage *)glassImage;

/**
 *  返回一个毛玻璃效果
 *
 *  @param level 等级值范围0~1.0
 */
- (UIImage *)glassImageWithLevel:(CGFloat)level;
/**
 *  根据颜色绘制一个圆角矩形图片
 */
+(instancetype)roundRectImageWithRGBColor:(UIColor*)color;
/**
 *  根据颜色和尺寸生成一张图片
 */
+ (UIImage*)imageWithColor:(UIColor *)color size:(CGSize)size;
/**
 *  缩放图片到指定的尺寸
 *  @param img  原始图片
 *  @param size 目标尺寸
 *
 *  @return 生成的图片
 */
+ (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size;

/**
 *  通过缩放图片和裁剪生成一张图片
 *  (以宽度和高度中小的一个作为依据)
 *  @param targetSize 目标尺寸
 *
 *  @return 生成的图片
 */
- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize;


@end
