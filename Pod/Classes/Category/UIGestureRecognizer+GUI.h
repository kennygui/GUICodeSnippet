//
//  UIGestureRecognizer+GUI.h
//  LTTachograph
//
//  Created by kenny on 12/11/15.
//  Copyright © 2015 kenny. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@interface UIGestureRecognizer (GUI)
/**
 *  the view which hold the recognizer.
 */
@property(nonatomic,weak)UIView* senderView;
@end
