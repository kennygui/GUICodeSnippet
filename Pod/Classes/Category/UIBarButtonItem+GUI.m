//
//  UIBarButtonItem+GQ.m
//  DoubleLineTracking
//
//  Created by guiqing on 12/6/14.
//  Copyright (c) 2014 guiqing. All rights reserved.
//

#import "UIBarButtonItem+GUI.h"

@implementation UIBarButtonItem (GUI)

+ (UIBarButtonItem *)itemWithIcon:(NSString *)icon highIcon:(NSString *)highIcon target:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:[UIImage imageNamed:icon] forState:UIControlStateNormal  ];
    [button setBackgroundImage:[UIImage imageNamed:highIcon] forState:UIControlStateHighlighted];
    //带着 left right 的元素,不用设置位置,所以,只有宽高即可
    button.frame = (CGRect){CGPointZero,button.currentBackgroundImage.size};
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return [[UIBarButtonItem alloc]initWithCustomView:button];
    
    
}


+ (UIBarButtonItem *)itemWithImageName:(NSString *)imageName target:(id)target action:(SEL)action
{
    UIButton *button = [[UIButton alloc] init];
    [button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
    // 设置按钮的尺寸为背景图片的尺寸
    CGSize size = button.currentBackgroundImage.size;

    CGRect frame = CGRectMake(button.frame.origin.x,button.frame.origin.y,size.width,size.height);
    button.frame = frame;
    // 监听按钮点击
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

@end
