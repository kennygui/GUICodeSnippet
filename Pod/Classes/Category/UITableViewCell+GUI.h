//
//  UITableViewCell+GUI.h
//  Pods
//
//  Created by kenny on 3/29/16.
//
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (GUI)

+ (void)registerTableView:(UITableView *)tableview
            nibIdentifier:(NSString *)identifier;

+ (CGFloat)cellHeightFromModel:(id)model atIndexPath:(NSIndexPath *)path;

- (void)configCell:(UITableViewCell *)cell
         withModel:(id)model
       atIndexPath:(NSIndexPath *)indexPath;

@end
