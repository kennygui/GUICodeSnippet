//
//  UILabel+GQ.h
//  CloudNurse
//
//  Created by katsura on 1/22/15.
//  Copyright (c) 2015 newone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (GUI)
+(UILabel*)labelWithString:(NSString*)str FontSize:(CGFloat)fontSize ;

/**
 *  根据给定的Font以及str计算UILabel的frameSize的方法
 *
 *
 */
-(CGSize)boundingRectWithSize:(CGSize)size;

-(CGSize)boundingRectWithSize:(CGSize)size font:(UIFont*) font;
@end
