//
//  UIGestureRecognizer+GUI.m
//  LTTachograph
//
//  Created by kenny on 12/11/15.
//  Copyright © 2015 kenny. All rights reserved.
//

#import "UIGestureRecognizer+GUI.h"

static NSString* senderViewKey = @"senderViewKey";

@implementation UIGestureRecognizer (GUI)

-(void)setSenderView:(UIView *)senderView{
    objc_setAssociatedObject(self, &senderViewKey, senderView,OBJC_ASSOCIATION_ASSIGN);
}

-(UIView *)senderView{
   return  objc_getAssociatedObject(self, &senderViewKey);
}

@end
