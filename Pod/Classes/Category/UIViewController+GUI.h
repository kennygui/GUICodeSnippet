//
//  UIViewController+GUI.h
//  CloudNurse
//
//  Created by Katsura on 6/30/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (GUI)
-(void)backItemWithNoTitle;
-(void)backItemWithTitle:(NSString*)title;
@end
