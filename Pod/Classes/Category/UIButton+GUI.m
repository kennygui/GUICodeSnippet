//
//  UIButton+GQ.m
//  CloudNurse
//
//  Created by katsura on 1/23/15.
//  Copyright (c) 2015 newone. All rights reserved.
//

#import "UIButton+GUI.h"
#import "UIImage+GUI.h"

@implementation UIButton (GUI)


+(UIButton*)buttonWithImage:(NSString*)normalImage highlightedImage:(NSString*)highlightedImage target:(id)target action:(SEL)action
{
    
    UIButton *button  = [[UIButton alloc]init];
    
    [button setImage:[UIImage imageNamed:normalImage] forState:UIControlStateNormal];
    
    [button setImage:[UIImage imageNamed:highlightedImage] forState:UIControlStateHighlighted];
    
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    

    return button;
}

+(UIButton*)buttonWithTitle:(NSString*)titleString highlightTitle:(NSString*)highlightedString target:(id)target action:(SEL)action
{
    
    UIButton *button  = [[UIButton alloc]init];
    
    [button setTitle:titleString forState:UIControlStateNormal];
    [button setTitle:highlightedString forState:UIControlStateHighlighted];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

/**
 *  添加按钮
 *
 *  @param icon  图标
 *  @param title 标题
 */
+(UIButton *)setupBtnWithIcon:(NSString *)icon title:(NSString *)title bgColor:(UIColor *)bgColor
{
    UIButton *btn = [[UIButton alloc] init];
//    btn.tag = self.subviews.count;
//    [btn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self addSubview:btn];

    // 设置图片和文字
    [btn setImage:[UIImage imageNamed:icon] forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:17];

    // 设置按钮选中的背景
    [btn setBackgroundImage:[UIImage imageWithColor:bgColor] forState:UIControlStateSelected];

    // 设置高亮的时候不要让图标变色
    btn.adjustsImageWhenHighlighted = NO;

    // 设置按钮的内容左对齐
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

    // 设置间距
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    btn.contentEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);

    return btn;
}

@end
