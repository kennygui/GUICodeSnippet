//
//  NSMutableArray+GQ.m
//  netBarRemoteCall
//
//  Created by Katsura on 3/24/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//

#import "NSMutableArray+GUI.h"

@implementation NSMutableArray (GUI)
- (void)insertArray:(NSArray *)newAdditions atIndex:(NSUInteger)index
{
    NSMutableIndexSet *indexes = [NSMutableIndexSet indexSet];
    for(NSInteger i = index;i < newAdditions.count+index;i++)
    {
        [indexes addIndex:i];
    }
    [self insertObjects:newAdditions atIndexes:indexes];
}
@end
