//
//  UIView+MansoryGUI.h
//  InTheCity
//
//  Created by Katsura on 4/25/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MansoryGUI)

/**
 *  水平方向排列 view
 *
 *  @param views 需要排列的view
 */
- (void) distributeSpacingHorizontallyWith:(NSArray*)views;

/**
 *  垂直方向排列 view
 *
 *  @param views 需要排列的view
 */
- (void) distributeSpacingVerticallyWith:(NSArray*)views;

@end
