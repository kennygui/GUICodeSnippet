//
//  GUIGlobalSystemManager.m
//  Pods
//
//  Created by kenny on 3/17/16.
//
//

#import "GUIGlobalSystemManager.h"

@implementation GUIGlobalSystemManager
/**
 *  获取Document目录
 *
 *  @return Document路径
 */
+ (NSString *)documentPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths lastObject];

    //另一种方式
    // [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    return documentPath;
}

/**
 *  获取Cache目录
 *  (Library/Cache)
 *  @return 目录
 */
+(NSString*)cachePath{

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDir = [paths objectAtIndex:0];
    return cachesDir;

}

/**
 *  控制器是否可见(是否正在显示当前的控制器)
 *
 *  @param viewController 判定的控制器
 *
 *  @return BOOL
 */
+ (BOOL)isViewControllerVisible:(UIViewController *)viewController {

    return (viewController.isViewLoaded && viewController.view.window);
}
@end
