//
//  GUIPathTool.swift
//  Pods
//
//  Created by kenny on 5/12/16.
//
//

import UIKit

class GUIPathTool: NSObject {
    var path:String{
        return NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).last!
    }
}
