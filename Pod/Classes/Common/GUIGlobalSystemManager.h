//
//  GUIGlobalSystemManager.h
//  Pods
//
//  Created by kenny on 3/17/16.
//
//

#import <Foundation/Foundation.h>

@interface GUIGlobalSystemManager : NSObject
/**
 *  获取Document目录
 *
 *  @return Document路径
 */
+(NSString*)documentPath;


/**
 *  获取Cache目录
 *  (Library/Cache)
 *  @return 目录
 */
+(NSString*)cachePath;


/**
 *  控制器是否可见(是否正在显示当前的控制器)
 *
 *  @param viewController 判定的控制器
 *
 *  @return BOOL
 */
+(BOOL)isViewControllerVisible:(UIViewController *)viewController;

@end
