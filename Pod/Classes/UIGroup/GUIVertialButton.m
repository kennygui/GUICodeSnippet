//
//  GUIVertialButton.m
//  LeCarDvr
//
//  Created by kenny on 3/7/16.
//  Copyright © 2016 com.letv.leauto. All rights reserved.
//

#import "GUIVertialButton.h"

@implementation GUIVertialButton

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView.contentMode = UIViewContentModeScaleToFill;
        [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
    }
    return self;
}

-(void)setImageSize:(CGSize)imageSize{
    _imageSize = imageSize;
    [self setNeedsDisplay];
}

-(void)setTitleSize:(CGSize)titleSize{
    _titleSize = titleSize;
    [self setNeedsDisplay];
    
}

-(void)setImageTopMargin:(CGFloat)imageTopMargin{
    _imageTopMargin = imageTopMargin;
    [self setNeedsDisplay];
}
-(CGRect)imageRectForContentRect:(CGRect)contentRect{
    
    CGFloat width = CGRectGetWidth(contentRect);
    CGFloat x     = (width-_imageSize.width)/2.0f;
    return CGRectMake(x,_imageTopMargin,_imageSize.width,_imageSize.height);
    
}


-(CGRect)titleRectForContentRect:(CGRect)contentRect{
    CGFloat width = CGRectGetWidth(contentRect);
    return CGRectMake((width-_titleSize.width)/2.0f,_imageTopMargin+_titleTopMargin+_imageSize.height+_titleTopMargin,_titleSize.width,_titleSize.height);
}

@end
