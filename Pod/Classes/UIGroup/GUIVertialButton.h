//
//  GUIVertialButton.h
//  LeCarDvr
//
//  Created by kenny on 3/7/16.
//  Copyright © 2016 com.letv.leauto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GUIVertialButton : UIButton
@property(nonatomic,assign)CGSize  imageSize;
@property(nonatomic,assign)CGSize  titleSize;
/**
 *  图片到顶部的距离
 */
@property(nonatomic,assign)CGFloat  imageTopMargin;
/**
 *  文字到图片底部的距离
 */
@property(nonatomic,assign)CGFloat  titleTopMargin ;


@end
