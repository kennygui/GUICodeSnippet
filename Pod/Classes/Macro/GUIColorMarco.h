//
//  GUIColorMarco.h
//  Pods
//
//  Created by kenny on 2/25/16.
//
//

#ifndef GUIColorMarco_h
#define GUIColorMarco_h

/**
 * 自定义颜色
 */
#define GUIColor(r, g, b) [UIColor colorWithRed:(r) / 255.0 green:(g) / 255.0 blue:(b) / 255.0 alpha:1.0]
/**
 *  带透明度的颜色
 */
#define GUIColorRGBA(r, g, b, a) [UIColor colorWithRed:(r) / 255.0 green:(g) / 255.0 blue:(b) / 255.0 alpha:a]
/**
 *  随机颜色
 */

#ifndef GUIRandomColor
#define GUIRandomColor GUIColor(arc4random_uniform(256), arc4random_uniform(256), arc4random_uniform(256))
#endif

/**
 *  16进制颜色
 */
#ifndef GUIColorFromRGB
#define GUIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 blue:((float)(rgbValue & 0xFF)) / 255.0 alpha:1.0]

#endif

#endif /* GUIColorMarco_h */
