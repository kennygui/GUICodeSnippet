//
//  GUIFrameMacro.h
//  baseProject
//
//  Created by katsura on 1/29/15.
//  Copyright (c) 2015 newone. All rights reserved.
//

#ifndef baseProject_GUIFrameMacro_h
#define baseProject_GUIFrameMacro_h

/**
 *  屏幕 frame
 */
#define GUIScreenFrame [UIScreen mainScreen].bounds
/**
 *  屏幕中心
 */
#define GUIScreenCenter CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2)

/**
 *  屏幕宽度
 */
#define GUIScreenWidth (GUIScreenFrame.size.width)

/**
 *  屏幕高度
 */
#define GUIScreenHeight (GUIScreenFrame.size.height)

/**
 *  间距
 */
#define kGlobalHMargin (GUIScreenFrame.size.width/32)
#define kGlobalVMargin (GUIScreenFrame.size.height/48)
/**
 *  宽高元素
 *
 */
#define kGlobalWidthFactor (GUIScreenFrame.size.width/320)
#define kGlobalHeightFactor (GUIScreenFrame.size.height/480)

/**
 *  statusbar 高度
 */
#define GUIStatusbarHeight (20)

/**
 *  导航栏高度
 */
#define GUINavigationBarHeight (44)

/**
 *  tabbar高度
 */
#define GUITabbarHeight (49)

/**
 *  不同设备
 */
#define kIP4ScreenSizeDevices ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

#define kIP5ScreenSizeDevices ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define kFour ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)

#define kiPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242,2208), [[UIScreen mainScreen] currentMode].size) : NO)

#define kIphone4or5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? 640==[[UIScreen mainScreen] currentMode].size.width: NO)




#endif
