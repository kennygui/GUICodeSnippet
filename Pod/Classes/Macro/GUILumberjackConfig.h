//
//  GQLumberjackConfig.h
//  InTheCity
//
//  Created by Katsura on 4/8/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//
/**
 *  本头文件用于配置 cocoalumberjack 框架的颜色.
 *  要使用控制台颜色打印需要:
 *  1. 安装 XcodeColor 插件
 *  2. 配置:
        2.1 scheme Editor (product ->Edit Scheme)
        2.2 选择 "Run" ,然后选择 Arguments tab 页
        2.3 添加一个新的环境变量: "XcodeColors" 值:"YES"
 *  3. 将 KDDLogConfig 写在 AppDelegate 的 didFinishLaunch 方法中.
 */

#ifndef baseProject_GQCocoaLumberjack_h
#define baseProject_GQCocoaLumberjack_h

#import "DDLog.h"
#import "DDTTYLogger.h"
#import "DDASLLogger.h"



/**
 *  此处指定了日志的显示级别,默认:
 *  调试: 打印所有
 *  发布: 关闭打印
 */
#ifdef DEBUG
static const int ddLogLevel =  DDLogLevelVerbose;
#else
static const int ddLogLevel =  DDLogLevelError;
#endif


/**
 *  将 KDDLogConfig 写在 AppDelegate 的 didFinishLaunch 方法中.
 */
#define kDDLogConfig  [DDLog addLogger:[DDTTYLogger sharedInstance]];\
[[DDTTYLogger sharedInstance] setColorsEnabled:YES];\
[[DDTTYLogger sharedInstance] setForegroundColor:[UIColor greenColor] backgroundColor:nil forFlag:LOG_FLAG_VERBOSE];\
[[DDTTYLogger sharedInstance] setForegroundColor:[UIColor yellowColor] backgroundColor:nil forFlag:LOG_FLAG_INFO];\
[[DDTTYLogger sharedInstance] setForegroundColor:[UIColor redColor] backgroundColor:nil forFlag:LOG_FLAG_ERROR];\
[[DDTTYLogger sharedInstance] setForegroundColor:[UIColor orangeColor] backgroundColor:nil forFlag:LOG_FLAG_WARN];


/**
 *  将传入的对象打印成字典
 */
#define kPrintDict(object) NSDictionary *dict = object.keyValues ;\
DDLogVerbose(@"%@",dict);


#endif
