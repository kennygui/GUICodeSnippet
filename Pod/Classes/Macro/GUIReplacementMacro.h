//
//  GUIReplacementMacro.h
//  CloudNurse
//
//  Created by Katsura on 7/10/15.
//  Copyright (c) 2015 Katsura. All rights reserved.
//

#ifndef CloudNurse_GUIReplacementMacro_h
#define CloudNurse_GUIReplacementMacro_h

/**
 *  单例代码
 */
#define kSingletonWithSingletonMethodName(methodName) \
+(instancetype)allocWithZone:(struct _NSZone *)zone\
{\
static id instance;\
static dispatch_once_t onceToken;\
dispatch_once(&onceToken, ^{\
instance = [super allocWithZone:zone];\
});\
return instance;\
}\
+(instancetype)sharedBO{\
return [[self alloc]init];\
}


/**
 *  返回按钮标题
 */

#define kBackItemTitle_back \
UIBarButtonItem *backItem=[[UIBarButtonItem alloc]init]; \
\
backItem.title= @"返回"; \
\
self.navigationItem.backBarButtonItem = backItem;

#define kBackItemTitle_noTitle \
UIBarButtonItem *backItem=[[UIBarButtonItem alloc]init]; \
\
backItem.title= @" "; \
\
self.navigationItem.backBarButtonItem = backItem;



#endif
