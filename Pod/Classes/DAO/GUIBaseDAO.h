//
//  GUIBaseDAO.h
//  Pods
//
//  Created by kenny on 3/17/16.
//
//

#import <Foundation/Foundation.h>
@class YapDatabaseConnection;

@interface GUIBaseDAO : NSObject
/**
 *  获取位于指定位置的数据库链接对象(如果没有则创建)
 *
 *  @param path 数据库位置
 *
 *  @return 链接对象
 */
+(YapDatabaseConnection*)databaseConnectionInpath:(NSString*)path;



@end
