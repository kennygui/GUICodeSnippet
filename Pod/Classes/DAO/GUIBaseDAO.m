//  Pods
//
//  Created by kenny on 3/17/16.
//
//

#import "GUIBaseDAO.h"
#import "YapDatabase.h"

@implementation GUIBaseDAO

/**
 *  获取位于指定位置的数据库链接对象(如果没有则创建)
 *
 *  @param path 数据库位置
 *
 *  @return 链接对象
 */
+(YapDatabaseConnection*)databaseConnectionInpath:(NSString*)path{
    YapDatabase * dataBase = [[YapDatabase alloc] initWithPath:path];
    YapDatabaseConnection* connection = [dataBase newConnection];
    return connection;
}
@end
