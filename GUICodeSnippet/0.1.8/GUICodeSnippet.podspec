#
# Be sure to run `pod lib lint GUICodeSnippet.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "GUICodeSnippet"
  s.version          = "0.1.8"
  s.summary          = "code segment of kennyGui"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
                      code segment of kenny ,and in most situation ,is for private use only
                       DESC

  s.homepage         = "https://git.oschina.net/kennygui/GUICodeSnippet"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "katsurake" => "katsurake@foxmail.com" }
  s.source           = { :git => "https://git.oschina.net/kennygui/GUICodeSnippet.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/*.h'
  s.resource_bundles = {
    'GUICodeSnippet' => ['Pod/Assets/*.png']
  }

  s.public_header_files = 'Pod/Classes/*.h'
  s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'CocoaLumberjack', '~> 2.2.0'
  s.dependency 'MBProgressHUD', '~> 0.9.2'
  s.dependency 'Masonry', '~> 0.6.4'
  s.dependency 'ReactiveCocoa', '~> 2.5'
  s.dependency 'YapDatabase', '~> 2.8.3'
  s.dependency 'DTCoreText', '~> 1.6.17'
  s.dependency 'MJExtension', '~> 3.0.10'


#Macro 模块
s.subspec 'Macro' do |macro|
macro.source_files = 'Pod/Classes/Macro/*.{h,m}'
macro.framework = 'UIKit'
#macro.dependency 'GUICodeSnippet/Macro'
end

#Category 模块
s.subspec 'Category' do |category|
category.source_files = 'Pod/Classes/category/*.{h,m}'
category.framework = 'UIKit'
category.dependency 'GUICodeSnippet/Macro'
end

#UIGroup  模块
s.subspec 'UIGroup' do |uigroup|
uigroup.source_files = 'Pod/Classes/uigroup/*.{h,m}'
uigroup.framework = 'UIKit'
uigroup.dependency 'GUICodeSnippet/Macro'
end

#DAO 模块
s.subspec 'DAO' do |dao|
dao.source_files = 'Pod/Classes/DAO/*.{h,m}'
dao.framework = 'UIKit'
dao.dependency 'GUICodeSnippet/Macro'
end

#DAO 模块
s.subspec 'Common' do |common|
common.source_files = 'Pod/Classes/Common/*.{h,m}'
common.framework = 'UIKit'
common.dependency 'GUICodeSnippet/Macro'
end


end
