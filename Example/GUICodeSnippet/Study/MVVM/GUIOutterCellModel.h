//
//  GUIOutterCellModel.h
//  GUICodeSnippet
//
//  Created by kenny on 3/29/16.
//  Copyright © 2016 katsurake. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GUIOutterCellModel : NSObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) CGFloat height;
@end
