//
//  GUIOutterTableDatasourceDelegate.h
//  GUICodeSnippet
//
//  Created by kenny on 3/29/16.
//  Copyright © 2016 katsurake. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CellConfigBlock)(NSIndexPath *indexPath, id Item, UITableViewCell *cell);
typedef CGFloat (^CellHeightBlock)(NSIndexPath *indexPath, id item);
typedef void (^DidSelectCellBlock)(NSIndexPath *indexPath, id item);

@interface GUIOutterViewModel : NSObject

-(id)initWithItems:(NSArray*)items reuseIdentifier:(NSString*)reuseIdentifier configBlock:(CellConfigBlock)cellConfig cellHeightBlock:(CellHeightBlock)cellHeight didSelect:(DidSelectCellBlock)didSelect;

-(void)dealTableViewDataSourceDelegate:(UITableView*)tableview;

-(id)itemAtIndexPath:(NSIndexPath*)indexPath;

@end
