//
//  GUIOutterDelegateController.m
//  GUICodeSnippet
//
//  Created by kenny on 3/29/16.
//  Copyright © 2016 katsurake. All rights reserved.
//

#import "GUIOutterDelegateController.h"
#import "GUIOutterCellModel.h"
#import "GUIOutterViewModel.h"
#import "GUIOutterCell.h"

@interface GUIOutterDelegateController ()
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@property(nonatomic,copy)NSArray * list;

@end

@implementation GUIOutterDelegateController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configTableView];
    
}




-(void)configTableView{
    self.listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    CellConfigBlock configCell= ^(NSIndexPath* indexPath,GUIOutterCellModel* model,UITableViewCell* cell){

    };

}




- (NSArray *)list
{
    if (!_list) {
         NSMutableArray * list = [NSMutableArray array] ;
        
        for (int i = 0; i < 10; i++)
        {
            GUIOutterCellModel * model = [GUIOutterCellModel new];
            model.title = [NSString stringWithFormat:@"my name is : %d",i] ;
            model.height = 50 + i * 5 ;
            [list addObject:model] ;
        }
        
        _list = [list copy];
    }
    
    return _list ;
}
@end
