//
//  GUIRegularExpressionController.m
//  GUICodeSnippet
//
//  Created by kenny on 3/15/16.
//  Copyright © 2016 katsurake. All rights reserved.
//

#import "GUIRegularExpressionController.h"
#import "NSString+GUI.h"
#import "ReactiveCocoa.h"

@interface GUIRegularExpressionController ()
@property (weak, nonatomic) IBOutlet UITextView *sourceTextView;
@property (weak, nonatomic) IBOutlet UITextView *TargetSourceTextView;

@property (weak, nonatomic) IBOutlet UITextView *regularTextView;

@end

@implementation GUIRegularExpressionController

- (void)viewDidLoad {
    [super viewDidLoad];
    @weakify(self);
    RACSignal * finalSignal = [RACSignal combineLatest:@[_sourceTextView.rac_textSignal,_regularTextView.rac_textSignal] reduce:^(NSString* sourceString,NSString * regularString){
        
        return @([sourceString length]>0&&[regularString length]>0);
    }];
    
    
    [finalSignal subscribeNext:^(id x) {
        @strongify(self);
        if ([x boolValue]) {
            NSArray *resultArray = [self.sourceTextView.text regularExpressionWithPatten:self.regularTextView.text];
            NSMutableString *string = [@"" mutableCopy];
            
            [resultArray enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL *_Nonnull stop) {
                [string appendFormat:@"%@\n", obj];
            }];
            self.TargetSourceTextView.text = string;
        }
    }];
    
    
    
    

}


@end
