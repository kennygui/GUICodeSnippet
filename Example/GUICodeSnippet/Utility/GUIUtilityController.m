//
//  GUIUtilityController.m
//  GUICodeSnippet
//
//  Created by kenny on 3/15/16.
//  Copyright © 2016 katsurake. All rights reserved.
//

#import "GUIUtilityController.h"
#import "GUICodeSnippet.h"

@interface GUIUtilityController ()
@property (nonatomic, copy) NSArray *titles;
@property (nonatomic, copy) NSArray *segues;

@end

@implementation GUIUtilityController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.titles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"REUID"];
    cell.backgroundColor = GUIRandomColor;
    [cell.textLabel setText:self.titles[indexPath.row]];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"REUID"];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [self performSegueWithIdentifier:self.segues[indexPath.row] sender:self];
}

- (NSArray *)titles {
    if (!_titles) {
        _titles = @[@"YapDataBase", @"RegularExpression",@"GUIOutterDelegateController"];
    }
    return _titles;
}

- (NSArray *)segues {
    if (!_segues) {
        _segues = @[@"YapDataBase", @"RegularExpression",@"GUIOutterDelegateController"];
    }
    return _segues;
}

@end
