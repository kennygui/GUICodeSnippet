//
//  GUIPerson.h
//  GUICodeSnippet
//
//  Created by kenny on 3/17/16.
//  Copyright © 2016 katsurake. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GUIBooks.h"

@interface GUIPerson : NSObject
@property (nonatomic, copy)NSString  *name;
@property (nonatomic, assign)NSInteger age;
@property(nonatomic,strong)GUIBooks *book;

@end
