//
//  GUIYapDataBaseController.h
//  GUICodeSnippet
//
//  Created by kenny on 3/17/16.
//  Copyright © 2016 katsurake. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GUIYapDataBaseController : UIViewController
- (IBAction)add:(id)sender;
- (IBAction)delete:(id)sender;
- (IBAction)update:(id)sender;

@end
