//
//  GUIYapDataBaseController.m
//  GUICodeSnippet
//
//  Created by kenny on 3/17/16.
//  Copyright © 2016 katsurake. All rights reserved.
//

#import "GUIYapDataBaseController.h"
#import "GUIBaseDAO.h"
#import "GUICodeSnippet.h"
#import "GUIPerson.h"
#import "GUIBooks.h"
#import "MJExtension.h"
#import "YapDatabase.h"

@interface GUIYapDataBaseController ()

@property (nonatomic, strong) YapDatabaseConnection *databaseConnection;
@end

@implementation GUIYapDataBaseController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *databasePath = [[GUIGlobalSystemManager documentPath] stringByAppendingPathComponent:@"total.sqlite"];
    self.databaseConnection = [GUIBaseDAO databaseConnectionInpath:databasePath];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)add:(id)sender {
    GUIPerson *person = [GUIPerson new];
    person.name = @"kenny";
    person.age = 23;

    GUIBooks *book = [GUIBooks new];
    book.name = @"pride and pridudice";
    book.price = 19.9;
    person.book = book;
    
    NSDictionary * personDict = [[person mj_keyValues] copy];
    
    [self.databaseConnection readWriteWithBlock:^(YapDatabaseReadWriteTransaction * _Nonnull transaction) {
        [transaction setObject:personDict forKey:@"person" inCollection:@"default"];
    }];
    
    
    
}

- (IBAction) delete:(id)sender {
    

}
- (IBAction)update:(id)sender {
}
- (IBAction)query:(id)sender {
    [self.databaseConnection readWithBlock:^(YapDatabaseReadTransaction * _Nonnull transaction) {
        NSLog(@"%@",[transaction objectForKey:@"person" inCollection:@"default"]);
    }];
}

@end
