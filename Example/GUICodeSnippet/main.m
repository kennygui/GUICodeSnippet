//
//  main.m
//  GUICodeSnippet
//
//  Created by katsurake on 02/19/2016.
//  Copyright (c) 2016 katsurake. All rights reserved.
//

@import UIKit;
#import "GUIAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GUIAppDelegate class]));
    }
}
