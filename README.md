# GUICodeSnippet

[![CI Status](http://img.shields.io/travis/katsurake/GUICodeSnippet.svg?style=flat)](https://travis-ci.org/katsurake/GUICodeSnippet)
[![Version](https://img.shields.io/cocoapods/v/GUICodeSnippet.svg?style=flat)](http://cocoapods.org/pods/GUICodeSnippet)
[![License](https://img.shields.io/cocoapods/l/GUICodeSnippet.svg?style=flat)](http://cocoapods.org/pods/GUICodeSnippet)
[![Platform](https://img.shields.io/cocoapods/p/GUICodeSnippet.svg?style=flat)](http://cocoapods.org/pods/GUICodeSnippet)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GUICodeSnippet is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "GUICodeSnippet"
```

## Author

katsurake, katsurake@foxmail.com

## License

GUICodeSnippet is available under the MIT license. See the LICENSE file for more info.

